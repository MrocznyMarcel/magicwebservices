//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MagicWebservices.Controllers.Mortis.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class mybb_reportedcontent
    {
        public long rid { get; set; }
        public long id { get; set; }
        public long id2 { get; set; }
        public long id3 { get; set; }
        public long uid { get; set; }
        public bool reportstatus { get; set; }
        public string reason { get; set; }
        public string type { get; set; }
        public long reports { get; set; }
        public string reporters { get; set; }
        public long dateline { get; set; }
        public long lastreport { get; set; }
    }
}
