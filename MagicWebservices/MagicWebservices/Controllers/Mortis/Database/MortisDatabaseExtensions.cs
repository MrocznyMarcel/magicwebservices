﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MagicWebservices.Controllers.Mortis.Database
{
    public static class MortisDatabaseExtensions
    {
        private static mybb_users unknownUser = new mybb_users() { uid = -1, username = "Nieznajomy" };

        public static mybb_newpoints_shop_items GetShopItemByIid(this MortisModelContainer database, int iid)
        {
            return database.mybb_newpoints_shop_items.First(x => x.iid == iid);
        }

        public static mybb_users GetUserByUid(this MortisModelContainer database, long uid)
        {
            return database.mybb_users.FirstOrDefault(x => x.uid == uid)
                   ?? unknownUser;
        }

        public static mybb_users GetUnknownUser(this MortisModelContainer database)
        {
            return unknownUser;
        }
    }
}