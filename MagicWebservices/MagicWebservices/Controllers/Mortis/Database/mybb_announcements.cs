//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MagicWebservices.Controllers.Mortis.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class mybb_announcements
    {
        public long aid { get; set; }
        public int fid { get; set; }
        public long uid { get; set; }
        public string subject { get; set; }
        public string message { get; set; }
        public long startdate { get; set; }
        public long enddate { get; set; }
        public bool allowhtml { get; set; }
        public bool allowmycode { get; set; }
        public bool allowsmilies { get; set; }
    }
}
