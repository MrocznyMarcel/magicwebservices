﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MagicWebservices.Controllers.Mortis.Database
{
    public class UserShopItems : ICollection<mybb_newpoints_shop_items>
    {
        #region Fields
        private Dictionary<mybb_newpoints_shop_categories, List<mybb_newpoints_shop_items>> itemsByCategory;
        #endregion
        #region Properties
        public int Count => itemsByCategory.Sum(x => x.Value.Count);
        public bool IsReadOnly => false;
        #endregion

        #region Constructors
        public UserShopItems(string shopItemsText)
        {
            itemsByCategory = GetItemsByCategoryFromText(shopItemsText);
        }
        #endregion
        #region Methods

        private Dictionary<mybb_newpoints_shop_categories, List<mybb_newpoints_shop_items>> GetItemsByCategoryFromText(string shopItemsText)
        {
            var itemsByCategory = new Dictionary<mybb_newpoints_shop_categories, List<mybb_newpoints_shop_items>>();
            if (!String.IsNullOrWhiteSpace(shopItemsText))
            {
                var presentItemsIds = UserShopItemHtml.GetItemIdsFromHtmlString(shopItemsText);
                using (var database = new MortisModelContainer())
                {
                    var presentItems = (from presentItemId in presentItemsIds.Distinct()
                        select database.GetShopItemByIid(presentItemId)).ToList();
                    var presentCategories = (from presentItemCategory in presentItems.Select(x => x.cid).Distinct()
                        select GetShopCategoryByCid(database, presentItemCategory)).ToList();
                    var itemCategory = presentItems.ToDictionary(presentItem => presentItem, presentItem => presentCategories.First(x => presentItem.cid == x.cid));
                    foreach (var item in itemCategory)
                    {
                        if (itemsByCategory.Keys.All(x => x.cid != item.Value.cid))
                           itemsByCategory[item.Value] = new List<mybb_newpoints_shop_items>();
                        itemsByCategory[item.Value].Add(item.Key);
                    }
                }
            }
            return itemsByCategory;
        }
        private static mybb_newpoints_shop_categories GetShopCategoryByCid(MortisModelContainer database, int cid)
        {
            return database.mybb_newpoints_shop_categories.First(x => x.cid == cid);
        }
        public IEnumerator<mybb_newpoints_shop_items> GetEnumerator()
        {
            List<mybb_newpoints_shop_items> itemsToIterate = new List<mybb_newpoints_shop_items>();
            foreach (var itemList in itemsByCategory.Values)
                itemsToIterate.AddRange(itemList);
            return itemsToIterate.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void Add(mybb_newpoints_shop_items item)
        {
            mybb_newpoints_shop_categories shopCategory = GetCategoryByCid(item.cid);
            if (!ItemsContainCategory(item.cid))
                itemsByCategory[shopCategory] = new List<mybb_newpoints_shop_items>();
            itemsByCategory[shopCategory].Add(item);
        }

        private mybb_newpoints_shop_categories GetCategoryByCid(int cid)
        {
            if (ItemsContainCategory(cid))
                return itemsByCategory.Keys.First(x => x.cid == cid);
            using (var database = new MortisModelContainer())
                return database.mybb_newpoints_shop_categories.First(x => x.cid == cid);
        }

        private bool ItemsContainCategory(int cid)
        {
            return itemsByCategory.Keys.Any(x => x.cid == cid);
        }

        public void Clear()
        {
            itemsByCategory.Clear();
        }

        public bool Contains(mybb_newpoints_shop_items item)
        {
            return itemsByCategory.Values.Any(x => x.Any(y => y.iid == item.iid));
        }

        public void CopyTo(mybb_newpoints_shop_items[] array, int arrayIndex)
        {
            foreach (var item in this)
                array[arrayIndex++] = item;
        }

        public bool Remove(mybb_newpoints_shop_items item)
        {
            mybb_newpoints_shop_categories shopCategory = GetCategoryByCid(item.cid);
            if (!ItemsContainCategory(item.cid))
                return false;
            item = itemsByCategory[shopCategory].FirstOrDefault(x => x.iid == item.iid);
            return item != null ? itemsByCategory[shopCategory].Remove(item) : false;
        }

        public override string ToString()
        {
            UserShopItemHtml html = new UserShopItemHtml();
            foreach (var categoryItems in itemsByCategory.Where(x => x.Value.Count > 0).OrderBy(x => x.Key.name))
            {
                mybb_newpoints_shop_categories category = categoryItems.Key;
                html.WriteCategory((int)category.cid, category.description, category.name);
                foreach (var item in categoryItems.Value.OrderBy(x => x.name))
                    html.WriteItem((int)item.iid, item.description, item.name);
            }
            return html.GetHtmlString();
        }

        #endregion

        private class UserShopItemHtml
        {
            #region Fields
            private const string itemsHeader = "<div class=\"guzik itemHeader\">Wyuczone zaklęcia</div>";
            private const string categoryIdOpen = "<strong class='categoryName'><span cid='";
            private const string categoryDescriptionAndNameOpen = "' original-title='";
            private const string categoryDescriptionClose = "'>";
            private const string categoryNameClose = "</span></strong><br/>";
            private const string listStart = "<ul style='list-style-type:square'>";
            private const string listItemIdOpen = "<li><span iid='";
            private const string listItemDescriptionAndNameOpen = "' original-title='";
            private const string listItemDescriptionClose = "'>";
            private const string listItemNameClose = "</span></li>";
            private const string listEnd = "</ul><br/>";
            private const string noItemsText = "Brak wyuczonych zaklęć.";

            private readonly StringBuilder htmlCode;
            private bool hasAnyItems;
            private bool isCategoryOpen;
            #endregion
            #region Constuctors
            public UserShopItemHtml()
            {
                htmlCode = new StringBuilder(itemsHeader);
                hasAnyItems = false;
                isCategoryOpen = false;
            }
            #endregion
            #region Methods
            public void WriteCategory(int categoryId, string categoryDescription, string categoryName)
            {
                if (isCategoryOpen)
                    htmlCode.Append($"{listEnd}");
                htmlCode.Append(
                    $"{categoryIdOpen}{categoryId}{categoryDescriptionAndNameOpen}{categoryDescription}{categoryDescriptionClose}{categoryName}{categoryNameClose}{listStart}");
                hasAnyItems = true;
                isCategoryOpen = true;
            }

            public void WriteItem(int itemId, string itemDescription, string itemName)
            {
                if (!isCategoryOpen)
                {
                    htmlCode.Append($"{listStart}");
                    isCategoryOpen = true;
                }
                htmlCode.Append(
                    $"{listItemIdOpen}{itemId}{listItemDescriptionAndNameOpen}{itemDescription}{listItemDescriptionClose}{itemName.Split('{')[0]}{listItemNameClose}");
            }

            public string GetHtmlString()
            {
                if (isCategoryOpen)
                    htmlCode.Append($"{listEnd}");
                if (!hasAnyItems)
                    htmlCode.Append($"{noItemsText}");
                return htmlCode.ToString();
            }

            public static IEnumerable<int> GetItemIdsFromHtmlString(string html)
            {
                return html.Split(new[] {listItemIdOpen}, StringSplitOptions.RemoveEmptyEntries).Skip(1)
                    .Select(text => Convert.ToInt32(text.Substring(0, text.IndexOf('\''))));
            }
            #endregion
        }
    }
}