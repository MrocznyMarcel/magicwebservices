//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MagicWebservices.Controllers.Mortis.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class mybb_adminsessions
    {
        public string sid { get; set; }
        public long uid { get; set; }
        public string loginkey { get; set; }
        public byte[] ip { get; set; }
        public long dateline { get; set; }
        public long lastactive { get; set; }
        public string data { get; set; }
        public string useragent { get; set; }
        public bool authenticated { get; set; }
    }
}
