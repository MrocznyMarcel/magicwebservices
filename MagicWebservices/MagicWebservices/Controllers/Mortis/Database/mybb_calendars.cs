//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MagicWebservices.Controllers.Mortis.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class mybb_calendars
    {
        public long cid { get; set; }
        public string name { get; set; }
        public int disporder { get; set; }
        public bool startofweek { get; set; }
        public bool showbirthdays { get; set; }
        public int eventlimit { get; set; }
        public bool moderation { get; set; }
        public bool allowhtml { get; set; }
        public bool allowmycode { get; set; }
        public bool allowimgcode { get; set; }
        public bool allowvideocode { get; set; }
        public bool allowsmilies { get; set; }
    }
}
