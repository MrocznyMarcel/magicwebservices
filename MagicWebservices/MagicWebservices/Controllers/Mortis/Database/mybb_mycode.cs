//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MagicWebservices.Controllers.Mortis.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class mybb_mycode
    {
        public long cid { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string regex { get; set; }
        public string replacement { get; set; }
        public bool active { get; set; }
        public int parseorder { get; set; }
    }
}
