//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MagicWebservices.Controllers.Mortis.Database
{
    using System;
    using System.Collections.Generic;
    
    public partial class mybb_polls
    {
        public long pid { get; set; }
        public long tid { get; set; }
        public string question { get; set; }
        public long dateline { get; set; }
        public string options { get; set; }
        public string votes { get; set; }
        public int numoptions { get; set; }
        public long numvotes { get; set; }
        public long timeout { get; set; }
        public bool closed { get; set; }
        public bool multiple { get; set; }
        public bool @public { get; set; }
        public int maxoptions { get; set; }
    }
}
