﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using MagicWebservices.Controllers.Mortis.Database;

namespace MagicWebservices.Controllers.Mortis.NewpointsShopManager
{
    public class NewpointsShopPointsChange
    {
        public int ManagerUid { get; set; }
        [Required(ErrorMessage = "Wybierz użytkownika")]
        public int Uid { get; set; }
        [Required(ErrorMessage = "Wprowadź ilość punktów")]
        public int PointsChange { get; set; }
        public string Reason { get; set; }
        public void ApplyPointsChange()
        {
            if (this.PointsChange == 0 || String.IsNullOrWhiteSpace(this.Reason)) return;           
            using (MortisModelContainer database = new MortisModelContainer())
            {
                mybb_users changedUser = database.GetUserByUid(this.Uid);
                changedUser.newpoints += PointsChange;
                database.nobu_shop_manager_log.Add(ConstructNobuLog(changedUser, this.PointsChange));
                database.SaveChanges();
            }
        }

        private nobu_shop_manager_log ConstructNobuLog(mybb_users user, int points)
        {
            return new nobu_shop_manager_log()
            {
                managerUid = this.ManagerUid,
                uid = (int)user.uid,
                spellId = 0,
                points = points,
                datetime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow,
                    TimeZoneInfo.FindSystemTimeZoneById("Central European Standard Time")),
                reason = this.Reason
            };
        }
    }
}