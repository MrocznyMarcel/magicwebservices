﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using MagicWebservices.Controllers.Mortis.Database;

namespace MagicWebservices.Controllers.Mortis.NewpointsShopManager
{
    public class NewpointsShopManagerData
    {
        #region Fields
        #endregion
        #region Properties
        public string CurrentUserUidAndLoginKey { get; set; }
        public mybb_users CurrentUser { get; set; }
        public NewpointsManagerPermissions CurrentUserPermissions { get; set; }
        public IList<mybb_users> UserList { get; set; }
        public IList<mybb_newpoints_shop_items> SpellList { get; set; }
        public IList<NewpointsShopLog> LogInfo { get; set; }
        public NewpointsShopPointsChange PointsChange { get; set; }
        public NewpointsShopSpellsChange SpellsChange { get; set; }
        #endregion
        #region Constructors
        public NewpointsShopManagerData() { }
        private NewpointsShopManagerData(MortisModelContainer database, NewpointsManagerPermissions permissions, mybb_users user)
        {
            this.CurrentUserPermissions = permissions;
            this.CurrentUser = user;
            this.PointsChange = new NewpointsShopPointsChange() {PointsChange = 0, Uid = -1};
            this.LogInfo = new List<NewpointsShopLog>();
            this.UserList = new List<mybb_users>();
            this.SpellList = new List<mybb_newpoints_shop_items>();
            if (permissions == NewpointsManagerPermissions.None) return;
            if (permissions != NewpointsManagerPermissions.SeeLog)
                this.UserList = database.mybb_users.OrderBy(u => u.username).ToList();
            if (permissions.HasFlag(NewpointsManagerPermissions.AddSpells) ||
                permissions.HasFlag(NewpointsManagerPermissions.SubtractSpells))
                this.SpellList = database.mybb_newpoints_shop_items.OrderBy(x => x.name).ToList();
            if (permissions.HasFlag(NewpointsManagerPermissions.SeeLog))
            {
                int recordsAmount = 100;
                List<nobu_shop_manager_log> nobuShopLogs = database.nobu_shop_manager_log.OrderByDescending(x => x.datetime).Take(recordsAmount).ToList();
                List<mybb_newpoints_log> newpointShopLogs = database.mybb_newpoints_log.Where(l => l.action == "shop_purchase").OrderByDescending(x => x.date).Take(recordsAmount).ToList();

                foreach (var nobuLog in nobuShopLogs)
                    this.LogInfo.Add(new NewpointsShopLog(nobuLog, database));
                foreach (var newpointsLog in newpointShopLogs)
                    this.LogInfo.Add(new NewpointsShopLog(newpointsLog, database));
                this.LogInfo = this.LogInfo.OrderByDescending(x => x.When).Take(recordsAmount).ToList();
            }
            this.LogInfo = this.LogInfo.ToList();
        }
        #endregion
        #region Methods
        public static NewpointsShopManagerData GetNewpointsShopManagerDataForToken(string uidAndLoginKey)
        {
            if (string.IsNullOrWhiteSpace(uidAndLoginKey)) uidAndLoginKey = "-1x";
            string[] uidAndLoginKeyArray = uidAndLoginKey.Split(new[] {'x'}, 2);
            int uid;
            bool uidParsed = int.TryParse(uidAndLoginKeyArray[0], out uid);
            if (!uidParsed) uid = -1;
            string loginKey = uidAndLoginKeyArray.Length > 1 ? uidAndLoginKeyArray[1] : "";

            using (var database = new MortisModelContainer())
            {
                mybb_users user = database.GetUserByUid(uid);
                if (user.loginkey == loginKey)
                    return new NewpointsShopManagerData(database, GetUserPermissions(database, user), user) { CurrentUserUidAndLoginKey = uidAndLoginKey };
                return new NewpointsShopManagerData(database, NewpointsManagerPermissions.None, database.GetUnknownUser());
            }
        }
        private static NewpointsManagerPermissions GetUserPermissions(MortisModelContainer database, mybb_users user)
        {
            nobu_shop_manager_permissions userPermission = database.nobu_shop_manager_permissions
                .Where(x => !x.IsGroup).FirstOrDefault(x => x.ID == user.uid);
            if (userPermission != null) return GetPermissions(userPermission);
            List<int> userGroupIds = GetUserGroupIds(user);
            List<nobu_shop_manager_permissions> userGroupPermissions =
                database.nobu_shop_manager_permissions.Where(x => x.IsGroup && userGroupIds.Contains(x.ID)).ToList();
            return MergePermissions(userGroupPermissions);
        }
        private static NewpointsManagerPermissions GetPermissions(nobu_shop_manager_permissions permissions)
        {
            var managerPermissions = NewpointsManagerPermissions.None;
            if (permissions.CanAddPoints)
                managerPermissions = NewpointsManagerPermissions.AddPoints;
            if (permissions.CanSubtractPoints)
                managerPermissions = managerPermissions | NewpointsManagerPermissions.SubtractPoints;
            if (permissions.CanAddSpells)
                managerPermissions = managerPermissions | NewpointsManagerPermissions.AddSpells;
            if (permissions.CanSubtractSpells)
                managerPermissions = managerPermissions | NewpointsManagerPermissions.SubtractSpells;
            if (permissions.CanSeeLog)
                managerPermissions = managerPermissions | NewpointsManagerPermissions.SeeLog;
            return managerPermissions;
        }
        private static List<int> GetUserGroupIds(mybb_users user)
        {
            return user.additionalgroups.Split(',').Select(int.Parse).Concat(new[] { user.usergroup }).ToList();
        }
        private static NewpointsManagerPermissions MergePermissions(List<nobu_shop_manager_permissions> permissions)
        {
            if (permissions.Count == 0) return NewpointsManagerPermissions.None;
            var allPermissions = permissions.Select(GetPermissions).ToList();

            var managerPermissions = NewpointsManagerPermissions.None;
            if (allPermissions.Any(x => x.HasFlag(NewpointsManagerPermissions.AddPoints)))
                managerPermissions = NewpointsManagerPermissions.AddPoints;
            if (allPermissions.Any(x => x.HasFlag(NewpointsManagerPermissions.SubtractPoints)))
                managerPermissions = managerPermissions | NewpointsManagerPermissions.SubtractPoints;
            if (allPermissions.Any(x => x.HasFlag(NewpointsManagerPermissions.AddSpells)))
                managerPermissions = managerPermissions | NewpointsManagerPermissions.AddSpells;
            if (allPermissions.Any(x => x.HasFlag(NewpointsManagerPermissions.SubtractSpells)))
                managerPermissions = managerPermissions | NewpointsManagerPermissions.SubtractSpells;
            if (allPermissions.Any(x => x.HasFlag(NewpointsManagerPermissions.SeeLog)))
                managerPermissions = managerPermissions | NewpointsManagerPermissions.SeeLog;
            return managerPermissions;
        }
        #endregion
    }

    [Flags]
    public enum NewpointsManagerPermissions
    {
        None = 1,
        AddPoints = 2,
        AddSpells = 4,
        SubtractPoints = 8,
        SubtractSpells = 16,
        SeeLog = 32
    }
}