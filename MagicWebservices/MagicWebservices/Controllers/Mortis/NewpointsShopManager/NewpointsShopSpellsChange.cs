﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MagicWebservices.Controllers.Mortis.Database;
using System.Text;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace MagicWebservices.Controllers.Mortis.NewpointsShopManager
{
    public class NewpointsShopSpellsChange
    {
        public int ManagerUid { get; set; }
        public int Uid { get; set; }
        public List<int> SpellsIids { get; set; }
        [DisplayName("Dodaj")]
        public bool Add { get; set; }
        public string Reason { get; set; }
        public void ApplySpellsChange()
        {
            if (string.IsNullOrWhiteSpace(this.Reason)) return;
            using (MortisModelContainer database = new MortisModelContainer())
            {
                var changedUser = database.GetUserByUid(this.Uid);
                var changedSpells =
                    database.mybb_newpoints_shop_items.Where(x => SpellsIids.Contains((int)x.iid));
                var spells = new NewpointsShopUserSpellList(changedUser.newpoints_items);
                if (this.Add)
                {
                    this.SpellsIids = this.SpellsIids.Where(x => !spells.SpellList.Contains(x)).ToList();
                    spells.SpellList.AddRange(this.SpellsIids);
                }
                else spells.SpellList.RemoveAll(x => SpellsIids.Contains(x));
                changedUser.newpoints_items = spells.Serialize();
                database.mybb_newpoints_log.AddRange(ConstructNewpointLogs(changedUser, SpellsIids));
                database.nobu_shop_manager_log.AddRange(ConstructNobuLogs(changedUser, SpellsIids)).Distinct();
                database.SaveChanges();
            }
        }

        private List<mybb_newpoints_log> ConstructNewpointLogs(mybb_users user, List<int> spellIds)
        {
            var logs = new List<mybb_newpoints_log>();
            foreach (var spellId in spellIds)
                logs.Add(this.ConstructNewpointLog(user, spellId));
            return logs;
        }

        private mybb_newpoints_log ConstructNewpointLog(mybb_users user, int spellId)
        {
            return new mybb_newpoints_log()
            {
                action = this.Add ? "manager_purchase" : "manager_delete",
                data = $"{spellId}-{0}-(first number = item id, second number = price)",
                date = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow,
                    TimeZoneInfo.FindSystemTimeZoneById("Central European Standard Time")).Millisecond,
                uid = user.uid,
                username = user.username
            };
        }

        private List<nobu_shop_manager_log> ConstructNobuLogs(mybb_users user, List<int> spellIds)
        {
            var logs = new List<nobu_shop_manager_log>();
            foreach (var spellId in spellIds)
                logs.Add(this.ConstructNobuLog(user, this.Add ? spellId : -spellId));
            return logs;
        }

        private nobu_shop_manager_log ConstructNobuLog(mybb_users user, int spellId)
        {
            return new nobu_shop_manager_log()
            {
                managerUid = this.ManagerUid,
                uid = (int)user.uid,
                spellId = spellId,
                points = 0,
                datetime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow,
                    TimeZoneInfo.FindSystemTimeZoneById("Central European Standard Time")),
                reason = this.Reason
            };
        }

        public class NewpointsShopUserSpellList
        {
            public List<int> SpellList { get; }

            public NewpointsShopUserSpellList(string toDeserialize)
            {
                this.SpellList = !string.IsNullOrWhiteSpace(toDeserialize) 
                    ? this.GetItemIidsList(this.GetSerializedItemList(toDeserialize))
                    : new List<int>();
            }

            public string Serialize()
            {
                if (SpellList.Count == 0) return string.Empty;
                var builder = new StringBuilder($"a:{SpellList.Count}:{{");
                for (int i = 0; i < this.SpellList.Count; i++)
                    builder.Append($"i:{i};s:{SpellList[i].ToString().Length}:\"{SpellList[i]}\";");
                builder.Append("}");
                return builder.ToString();
            }

            private string GetSerializedItemList(string toDeserialize)
            {
                return toDeserialize.Split('{')[1].TrimEnd('}');
            }

            private List<int> GetItemIidsList(string serializedItemList)
            {
                return serializedItemList.Split(';').Skip(1).
                    Where((x, y) => y % 2 == 0).Select(x => x.Split(':')[2].Trim('"'))
                    .Select(int.Parse).ToList();
            }
        }
    }
}