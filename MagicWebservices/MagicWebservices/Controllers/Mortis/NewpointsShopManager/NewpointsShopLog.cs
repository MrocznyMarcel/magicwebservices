﻿using MagicWebservices.Controllers.Mortis.Database;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MagicWebservices.Controllers.Mortis.NewpointsShopManager
{
    public class NewpointsShopLog
    {
        [DisplayName("Kto")]
        public string ManagerUsername { get; set; }
        [DisplayName("Komu")]
        public string Username { get; set; }
        [DisplayName("Co")]
        public string What { get; set; }
        [DisplayName("Kiedy")]
        public DateTime When { get; set; }
        public string Why { get; set; }
        public NewpointsShopLog(nobu_shop_manager_log log, MortisModelContainer database)
        {
            this.ManagerUsername = database.GetUserByUid(log.managerUid).username;
            this.Username = database.GetUserByUid(log.uid).username;
            this.What = log.spellId == 0 ? this.FormatPoints(log.points) 
                : this.FormatSpell(database, log.spellId);
            this.When = log.datetime.ToUniversalTime();
            this.Why = log.reason;
        }

        public NewpointsShopLog(mybb_newpoints_log log, MortisModelContainer database)
        {
            this.ManagerUsername = log.username;
            this.Username = log.username;
            this.What = $"Zakupił zaklęcie: {database.GetShopItemByIid(int.Parse(log.data.Split('-')[0])).name}";
            this.When = this.UnixTimeStampToDateTime((double) log.date).ToUniversalTime().AddHours(1);
            this.Why = string.Empty;
        }
        private DateTime UnixTimeStampToDateTime(double unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            System.DateTime dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        private string FormatPoints(int pointsChange)
        {
            return pointsChange >= 0 ? $"dodał {pointsChange} {FormatPointWord(pointsChange)}"
                : $"odjął {-pointsChange} {FormatPointWord(-pointsChange)}";
        }

        private string FormatPointWord(int pointsChange)
        {
            if (pointsChange < 20)
                switch (pointsChange)
                {
                    case 1: return "punkt";
                    case 2:
                    case 3:
                    case 4: return "punkty";
                    default: return "punktów"; 
                }
            else
                switch (pointsChange.ToString().Last())
                {
                    case '2':
                    case '3': return "punkty";
                    default: return "punktów";
                }
        }

        private string FormatSpell(MortisModelContainer database, int spellId)
        {
            var spell = database.GetShopItemByIid(Math.Abs(spellId));
            return spellId > 0 ? $"dodał zaklęcie {spell.name}" : $"usunął zaklęcie {spell.name}";
        }
    }
}