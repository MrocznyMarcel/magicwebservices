﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using HtmlAgilityPack;
using MagicWebservices.Controllers.Mortis.Database;
using MagicWebservices.Controllers.Mortis.NewpointsShopManager;
using MagicWebservices.Controllers.Utilities;
using MagicWebservices.Models;

namespace MagicWebservices.Controllers.Mortis
{
    public static class MortisTasks
    {
        public static class OnDemand
        {
            #region GetWrongPosts

            public static Report GetWrongPostsReport(int hours)
            {
                long timeInSeconds = hours * 3600;
                long timeStampsHoursAgo = GetUtcNowTimestamp() - timeInSeconds;
                var report = new StringBuilder("Posts with HTML issues:\n");
                bool anyIssues = false;
                using (var database = new MortisModelContainer())
                {
                    IEnumerable<mybb_posts> postsToCheck =
                        from post in database.mybb_posts
                        where post.dateline < timeStampsHoursAgo
                        select post;
                    try
                    {
                        foreach (mybb_posts mybbPost in postsToCheck)
                        {
                            var document = new HtmlDocument();
                            document.LoadHtml(mybbPost.message);
                            if (document.ParseErrors.Any())
                            {
                                anyIssues = true;
                                report.AppendLine($"There are problems with: https://mortis.org.pl/showthread.php?tid={mybbPost.tid}&pid={mybbPost.pid}#{mybbPost.pid}:");
                                foreach (HtmlParseError documentParseError in document.ParseErrors)
                                    report.AppendLine($"\t{documentParseError.Reason}");
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        anyIssues = false;
                        report = new StringBuilder("Exception occured while trying to check posts.\n");
                        report.AppendLine(e.Message);
                        report.AppendLine(e.StackTrace);
                    }
                }
                if (!anyIssues) report.AppendLine($"There are no html issues in posts from last {hours} hours");
                return new Report
                {
                    Name = "Wrong posts report",
                    Data = report.ToString(),
                };
            } 
            #endregion

            public static Report RebuildSpells()
            {
                using (var database = new MortisModelContainer())
                {
                    List<mybb_users> users = database.mybb_users.ToList();
                    List<mybb_userfields> userFields = database.mybb_userfields.ToList();
                    List<mybb_newpoints_shop_items> shopItems = database.mybb_newpoints_shop_items.ToList();
                    int index = 0;
                    foreach (mybb_users user in users)
                    {
                        mybb_userfields userField = userFields.FirstOrDefault(uf => uf.ufid == user.uid);
                        if (userField == null)
                            continue;

                        var userItems = new NewpointsShopSpellsChange.NewpointsShopUserSpellList(user.newpoints_items);
                        var userShopItems = new UserShopItems("Brak wyuczonych zaklęć.");
                        foreach (int spellId in userItems.SpellList)
                        {
                            mybb_newpoints_shop_items shopItem = shopItems.FirstOrDefault(x => x.iid == spellId);

                            if (shopItem != null)
                                userShopItems.Add(shopItem);
                        }
     
                        userField.fid22 = userShopItems.ToString();
                        if (index++ % 50 == 0)
                            database.SaveChanges();
                    }
                    database.SaveChanges();
                }
                return new Report
                {
                    Name = "Duplicates report",
                    Data = "Spells rebuilt."
                };
            }

            public static Report ChangePostAuthor(long postId, int newUserId)
            {
                try
                {
                    using (var database = new MortisModelContainer())
                    {
                        mybb_users newUser = database.GetUserByUid(newUserId);
                        mybb_posts post = database.mybb_posts.First(x => x.pid == postId);
                        mybb_users oldUser = database.GetUserByUid(post.uid);

                        post.uid = newUser.uid;
                        post.username = newUser.username;
                        oldUser.postnum--;
                        newUser.postnum++;

                        mybb_threads thread = database.mybb_threads.FirstOrDefault(x => x.firstpost == post.pid);
                        if (thread != null)
                        {
                            thread.uid = newUser.uid;
                            thread.username = newUser.username;
                        }
                        database.SaveChanges();
                    }
                }
                catch (Exception e)
                {
                    return new Report
                    {
                        Name = "Post author change",
                        Data = $"Post author change failed due to an error: {e.Message}"
                    };
                }
                return new Report
                {
                    Name = "Post author change",
                    Data = "Post author changed."
                };
            }
        }

        public static class FiveMinutes
        {
            #region UpdateProfileShopItems
            public static void UpdateProfileShopItems()
            {
                using (var database = new MortisModelContainer())
                {
                    var shopLogs = GetUnhandledShopLogs(database).ToList();
                    Dictionary<mybb_userfields, List<mybb_newpoints_shop_items>> userItemsToAdd = GetUserItemsToAdd(shopLogs, database);
                    Dictionary<mybb_userfields, List<mybb_newpoints_shop_items>> userItemsToDelete = GetUserItemsToDelete(shopLogs, database);
                    foreach (var userItemToAdd in userItemsToAdd)
                    {
                        var userShopItems = new UserShopItems(userItemToAdd.Key.fid22);
                        foreach (var item in userItemToAdd.Value)
                            userShopItems.Add(item);
                        userItemToAdd.Key.fid22 = userShopItems.ToString();
                    }
                    foreach (var userItemToDelete in userItemsToDelete)
                    {
                        var userShopItems = new UserShopItems(userItemToDelete.Key.fid22);
                        foreach (var item in userItemToDelete.Value)
                            userShopItems.Remove(item);
                        userItemToDelete.Key.fid22 = userShopItems.ToString();
                    }
                    SetShopLogsAsHandled(database, shopLogs);
                    database.SaveChanges();
                }
            }

            private static IEnumerable<mybb_newpoints_log> GetUnhandledShopLogs(MortisModelContainer database)
            {
                var handled = from nobuLog in database.nobu_shop_log
                              where nobuLog.handled
                              select nobuLog.lid;
                return from log in database.mybb_newpoints_log
                       where !handled.Contains((int)log.lid)
                       select log;
            }

            private static Dictionary<mybb_userfields, List<mybb_newpoints_shop_items>> GetUserItemsToAdd(
                IEnumerable<mybb_newpoints_log> shopLogs, MortisModelContainer database)
            {
                var userItemsToAdd = new Dictionary<mybb_userfields, List<mybb_newpoints_shop_items>>();
                foreach (var shopLog in shopLogs.Where(log => log.action.EndsWith("purchase")))
                {
                    mybb_userfields userFields = GetUserFieldsByUid(database, (int)shopLog.uid, userItemsToAdd);
                    if (!userItemsToAdd.ContainsKey(userFields))
                        userItemsToAdd[userFields] = new List<mybb_newpoints_shop_items>();
                    userItemsToAdd[userFields].Add(database.GetShopItemByIid(GetIidFromData(shopLog.data)));
                }
                return userItemsToAdd;
            }

            private static Dictionary<mybb_userfields, List<mybb_newpoints_shop_items>> GetUserItemsToDelete(
                IEnumerable<mybb_newpoints_log> shopLogs, MortisModelContainer database)
            {
                var userItemsToDelete = new Dictionary<mybb_userfields, List<mybb_newpoints_shop_items>>();
                foreach (var shopLog in shopLogs.Where(log => log.action.EndsWith("delete")))
                {
                    mybb_userfields userFields = GetUserFieldsByUid(database, (int)shopLog.uid, userItemsToDelete);
                    if (!userItemsToDelete.ContainsKey(userFields))
                        userItemsToDelete[userFields] = new List<mybb_newpoints_shop_items>();
                    userItemsToDelete[userFields].Add(database.GetShopItemByIid(GetIidFromData(shopLog.data)));
                }
                return userItemsToDelete;
            }
            private static mybb_userfields GetUserFieldsByUid(MortisModelContainer database, int uid, Dictionary<mybb_userfields, List<mybb_newpoints_shop_items>> dictionary = null)
            {
                mybb_userfields dictionaryUserfields = dictionary?.Keys.FirstOrDefault(x => x.ufid == uid);
                return dictionaryUserfields ?? database.mybb_userfields.First(x => x.ufid == uid);
            }

            private static int GetIidFromData(string data)
            {
                return Convert.ToInt32(data.Split('-')[0]);
            }

            public static void SetShopLogsAsHandled(MortisModelContainer database, IEnumerable<mybb_newpoints_log> handledShopLogs)
            {
                foreach (var handledShopLog in handledShopLogs)
                    database.nobu_shop_log.Add(new nobu_shop_log()
                    {
                        handled = true,
                        lid = (int)handledShopLog.lid
                    });
            }
            #endregion

            #region UpdateDepositLinks
            public static void UpdateDepositLinks()
            {
                using (var database = new MortisModelContainer())
                {
                    IEnumerable<mybb_userfields> userFieldsToUpdate = GetUserFieldsWithNoDepositLink(database, textForNoDeposit);
                    foreach (var userFieldToUpdate in userFieldsToUpdate)
                    {
                        mybb_posts depositPostOfUser = GetDepositPostOfUser(database, userFieldToUpdate.ufid);
                        userFieldToUpdate.fid12 = depositPostOfUser != null ? 
                            ConstructLinkForDeposit(depositPostOfUser.tid, depositPostOfUser.subject) :
                            textForNoDeposit;
                    }
                    database.SaveChanges();
                }
            }

            private static IEnumerable<mybb_userfields> GetUserFieldsWithNoDepositLink(MortisModelContainer database, string textForNoDeposit)
            {
                List<long> npcIds = GetNPCsIds(database);
                return database.mybb_userfields
                    .Where(userFields => userFields.fid12 == String.Empty || userFields.fid12 == null || userFields.fid12 == textForNoDeposit)
                    .Where(uf => !npcIds.Contains(uf.ufid))
                    .ToList();
            }

            private static mybb_posts GetDepositPostOfUser(MortisModelContainer database, long uid)
            {
                return
                    database.mybb_posts.FirstOrDefault(
                        x => x.visible &&
                        x.uid == uid &&
                        (x.fid == 53 || x.fid == 19) &&
                        (x.pid == x.replyto || x.replyto == 0));
            }

            private static string ConstructLinkForDeposit(long tid, string threadSubject) => $@"<a href='https://mortis.org.pl/showthread.php?tid={tid}' original-title=''>{threadSubject}</a>";
            private const string textForNoDeposit = "Brak skrytki";
            #endregion

            #region UpdateOwlMailLinks
            public static void UpdateOwlMailLinks()
            {
                using (var database = new MortisModelContainer())
                {
                    IEnumerable<mybb_userfields> userFieldsToUpdate = GetUserFieldsWithNoOwlMailLink(database, textForNoMail);
                    foreach (var userFieldToUpdate in userFieldsToUpdate)
                    {
                        mybb_posts owlMailPostOfUser = GetOwlMailPostOfUser(database, userFieldToUpdate.ufid);
                        userFieldToUpdate.fid13 = owlMailPostOfUser != null ?
                            ConstructTextForOwlMail(owlMailPostOfUser.tid) :
                            textForNoMail;
                    }
                    database.SaveChanges();
                }
            }

            private static IEnumerable<mybb_userfields> GetUserFieldsWithNoOwlMailLink(MortisModelContainer database, string textForNoOwlMail)
            {
                List<long> npcIds = GetNPCsIds(database);
                return database.mybb_userfields
                    .Where(userFields => userFields.fid13 == String.Empty || userFields.fid13 == null || userFields.fid13 == textForNoOwlMail)
                    .Where(uf => !npcIds.Contains(uf.ufid))
                    .ToList();
            }

            private static mybb_posts GetOwlMailPostOfUser(MortisModelContainer database, long uid)
            {
                return
                    database.mybb_posts.FirstOrDefault(
                        x => x.visible &&
                        x.uid == uid &&
                        (x.fid == 105 || x.fid == 21) &&
                        (x.pid == x.replyto || x.replyto == 0));
            }

            private static string ConstructTextForOwlMail(long tid) => $@"<a href='https://mortis.org.pl/showthread.php?tid={tid}' original-title=''>Sowia poczta</a>";
            private const string textForNoMail = "Brak poczty";
            #endregion

            #region UpdateDiaryLinks
            public static void UpdateDiaryLinks()
            {
                using (var database = new MortisModelContainer())
                {
                    IEnumerable<mybb_userfields> userFieldsToUpdate = GetUserFieldsWithNoDiaryLink(database, textForNoDiary);
                    foreach (var userFieldToUpdate in userFieldsToUpdate)
                    {
                        mybb_posts diaryPostOfUser = GetDiaryPostOfUser(database, userFieldToUpdate.ufid);
                        userFieldToUpdate.fid30 = diaryPostOfUser != null ?
                            ConstructTextForDiary(diaryPostOfUser.tid) :
                            textForNoDiary;
                    }
                    database.SaveChanges();
                }
            }

            private static IEnumerable<mybb_userfields> GetUserFieldsWithNoDiaryLink(MortisModelContainer database, string textForNoDiary)
            {
                List<long> npcIds = GetNPCsIds(database);
                return database.mybb_userfields
                    .Where(userFields => userFields.fid30 == String.Empty || userFields.fid30 == null || userFields.fid30 == textForNoDiary)
                    .Where(uf => !npcIds.Contains(uf.ufid))
                    .ToList();
            }

            private static mybb_posts GetDiaryPostOfUser(MortisModelContainer database, long uid)
            {
                return
                    database.mybb_posts.FirstOrDefault(
                        x => x.visible &&
                        x.uid == uid &&
                        (x.fid == 110 || x.fid == 24) &&
                        (x.pid == x.replyto || x.replyto == 0));
            }
            private static string ConstructTextForDiary(long tid) => $@"<a href='https://mortis.org.pl/showthread.php?tid={tid}' original-title=''>Dziennik</a>";
            private const string textForNoDiary = "Brak dziennika";
            #endregion

            private static List<long> GetNPCsIds(MortisModelContainer database)
                    => database.mybb_users.Where(u => u.usergroup == 15 || u.additionalgroups.Contains("15")).Select(u => u.uid).ToList();
        }

        public static class TwentyFourHours
        {
            private const string mortisBackupUrl = "ftp://mortis.org.pl/mortis/admin/backups/";
            private const string megaEmail = "pat366@gmail.com";
            private const string megaPassword = "ce191919f87e2e672dcdbd0715ff58f20eeefdaf";
            private const string megaFolderId = "VGZk1Q6R";
            
            #region MoveAllDatabaseBackupsToMegaDrive
            public static void MoveAllDatabaseBackupsToMegaDrive()
            {
                var mortisCredential = new NetworkCredential(@"nl1-wss1/mortisor", "Kaylin@W1917");
                string[] fileNames = GetBackupFileNames(mortisCredential);
                foreach (var fileName in fileNames)
                {
                    SendBackupFileToMegaDrive(fileName, mortisCredential);
                    DeleteBackupFileFromMortis(fileName, mortisCredential);
                }
            }

            private static string[] GetBackupFileNames(NetworkCredential mortisCredential)
            {
                var request = (FtpWebRequest)WebRequest.Create(mortisBackupUrl);
                request.Method = WebRequestMethods.Ftp.ListDirectory;
                request.Credentials = mortisCredential;

                using (var listDirectory = (FtpWebResponse)request.GetResponse())
                using (var reader = new StreamReader(listDirectory.GetResponseStream()))
                    return reader.ReadToEnd().Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries).Where(x => x.EndsWith(".sql")).ToArray();
            }

            private static void SendBackupFileToMegaDrive(string fileName, NetworkCredential mortisCredential)
            {
                var request = (FtpWebRequest)WebRequest.Create($"{mortisBackupUrl}{fileName}");
                request.Method = WebRequestMethods.Ftp.DownloadFile;
                request.Credentials = mortisCredential;
                
                using (var response = (FtpWebResponse)request.GetResponse())
                using (Stream responseStream = response.GetResponseStream())
                using (MemoryStream memoryStream = CreateMemoryStream(responseStream))
                using (var megaDrive = new MegaDriveConnector(megaEmail, megaPassword))
                    megaDrive.Upload(memoryStream, fileName, megaFolderId);
            }

            private static MemoryStream CreateMemoryStream(Stream stream)
            {
                int streamByte = stream.ReadByte();
                var streamBytes = new List<byte>();
                while (streamByte != -1)
                {
                    streamBytes.Add((byte)streamByte);
                    streamByte = stream.ReadByte();
                }
                return new MemoryStream(streamBytes.ToArray());
            }

            private static void DeleteBackupFileFromMortis(string fileName, NetworkCredential mortisCredential)
            {
                var request = (FtpWebRequest)WebRequest.Create($"{mortisBackupUrl}{fileName}");
                request.Method = WebRequestMethods.Ftp.DeleteFile;
                request.Credentials = mortisCredential;

                request.GetResponse();
            }
            #endregion

            #region DeletePostsOlderThanTwoWeeksInThread

            public static void DeletePostsOlderThanTwoWeeksInThread()
            {
                long utcNowTimestamp = MortisTasks.GetUtcNowTimestamp();
                long twoWeeksInSeconds = MortisTasks.GetSecondsFromDays(14);
                long timestampTwoWeeksAgo = utcNowTimestamp - twoWeeksInSeconds;
                using (var database = new MortisModelContainer())
                {
                    IEnumerable<mybb_posts> postsToDelete =
                        from post in database.mybb_posts
                        where post.tid == 1827 
                            && post.pid != 5483
                            && post.dateline < timestampTwoWeeksAgo
                        select post;
                    database.mybb_posts.RemoveRange(postsToDelete);
                    database.SaveChanges();
                }    
            }
            #endregion
        }
        private static long GetUtcNowTimestamp()
            => (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

        private static long GetSecondsFromDays(int days)
            => days * 86400;
    }
}