﻿using System;

namespace MagicWebservices.Controllers.Processes
{
    public class Process : IProcess
    {
        #region Fields
        private Action task;
        private ProcessResult lastResult;
        #endregion
        #region Properties
        public int ProcessId { get; } = ProcessIdProvider.GenerateProcessId();
        public string Name { get; }
        public IProcessResult CurrentResult => lastResult;
        #endregion
        #region Constructors
        public Process(Action task)
        {
            this.Name = task.Method.Name;
            this.task = task;
            this.lastResult = new ProcessResult(this.Name);
        }
        #endregion
        #region Methods
        public IProcessResult Execute()
        {
            try
            {
                this.task();
                this.lastResult = new ProcessResult(this.Name);
                return this.lastResult;
            }
            catch (Exception e)
            {
                this.lastResult = new ProcessResult(this.Name, false, e);
                return this.lastResult;
            }
        }
        #endregion
    }
}