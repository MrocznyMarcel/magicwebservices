﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MagicWebservices.Controllers.Processes
{
    public class SingleProcessResult : IProcessResult
    {
        #region Fields
        private DateTime startTime;
        private DateTime endTime;
        #endregion

        #region Properties
        public string Name { get; }
        public ProcessResultState State { get; }
        public Exception Error { get; }

        public DateTime StartTime
        {
            get { return startTime.ToUniversalTime(); }
            set
            {
                startTime = TimeZoneInfo.ConvertTimeFromUtc(value,
                    TimeZoneInfo.FindSystemTimeZoneById("Central European Standard Time"));
            }
        }
        public DateTime EndTime
        {
            get { return endTime.ToUniversalTime(); }
            set
            {
                endTime = TimeZoneInfo.ConvertTimeFromUtc(value,
                    TimeZoneInfo.FindSystemTimeZoneById("Central European Standard Time"));
            }
        }
        #endregion

        #region Constructors
        public SingleProcessResult(string name, ProcessResultState state, Exception error = null)
        {
            this.Name = name;
            this.State = state;
            this.Error = error;
        }
        #endregion

        #region Methods
        public string ResultMessage()
        {
            switch (this.State)
            {
                case "Success":
                    return $"{Name} - Sucess ({StartTime} - {EndTime})";
                case "Running":
                    return $"{Name} - Running ({StartTime})";
                case "Fail":
                    return $"{Name} - Fail ({StartTime} - {EndTime}) - {Error.Message} - {Error.StackTrace}";
                case "NeverRun":
                    return $"{Name} - Never run before";
            }
            return "Unexpected state.";
        }
        #endregion

    }
}