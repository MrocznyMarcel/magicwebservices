﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace MagicWebservices.Controllers.Processes
{
    public class SingleProcess : IProcess
    {
        #region Fields
        private readonly Action task;
        private DateTime startTime;
        private DateTime endTime;
        private bool isExecuting;
        private Task actionTask;
        private SingleProcessResult lastResult;
        private object isExecutingLock = new object();
        #endregion
        #region Properties
        public int ProcessId { get; } = ProcessIdProvider.GenerateProcessId();
        public string Name { get; }

        public IProcessResult CurrentResult
        {
            get
            {
                if (isExecuting)
                    return new SingleProcessResult(this.Name, ProcessResultState.Running) { StartTime = this.startTime };
                return lastResult;
            }
        }

        #endregion
        #region Constructors
        public SingleProcess(Action task)
        {
            this.Name = task.Method.Name;
            this.task = task;
            this.lastResult = new SingleProcessResult(this.Name, ProcessResultState.NeverRun);
        }
        #endregion
        #region Methods
        public IProcessResult Execute()
        {
            if (this.isExecuting) return new SingleProcessResult(this.Name, ProcessResultState.Running) { StartTime = this.startTime };
            lock (this.isExecutingLock)
            {
                
                this.startTime = DateTime.UtcNow;
                this.isExecuting = true;
                this.actionTask = Task.Run(() =>
                {
                    try
                    {
                        this.task();
                        this.endTime = DateTime.UtcNow;
                        this.lastResult = new SingleProcessResult(this.Name, ProcessResultState.Success)
                        {
                            StartTime = this.startTime,
                            EndTime = this.endTime
                        };
                        this.isExecuting = false;
                    }
                    catch (Exception e)
                    {
                        this.endTime = DateTime.UtcNow;
                        this.isExecuting = false;
                        this.lastResult = new SingleProcessResult(this.Name, ProcessResultState.Fail, e)
                        {
                            StartTime = this.startTime,
                            EndTime = this.endTime
                        };
                    }

                });
                return this.actionTask.Status == TaskStatus.Running
                    ? new SingleProcessResult(this.Name, ProcessResultState.Running) {StartTime = this.startTime}
                    : lastResult;
            }
        }
        #endregion
    }
}