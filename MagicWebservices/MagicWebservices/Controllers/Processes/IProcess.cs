﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicWebservices.Controllers.Processes
{
    public interface IProcess
    {
        int ProcessId { get; }
        string Name { get; }
        IProcessResult CurrentResult { get; }
        IProcessResult Execute();
    }
}
