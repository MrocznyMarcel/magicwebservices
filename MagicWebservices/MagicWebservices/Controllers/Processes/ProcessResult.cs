﻿using System;
using Microsoft.Ajax.Utilities;

namespace MagicWebservices.Controllers.Processes
{
    public class ProcessResult : IProcessResult
    {
        #region Fields
        #endregion
        #region Properties
        public string Name { get; }
        public ProcessResultState State { get; }
        public Exception Error { get; }
        #endregion
        #region Constructors
        public ProcessResult(string name, bool success = true, Exception error = null)
        {
            this.Name = name;
            this.State = success;
            this.Error = error;
        }
        #endregion        
        #region Methods
        public string ResultMessage()
        {
            return this.State == ProcessResultState.Success ? $"{this.Name} - Sucess" 
                : $"{this.Name} - Fail - {this.Error.Message} - {this.Error.StackTrace}";
        }
        #endregion
    }
}