﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MagicWebservices.Controllers.Processes
{
    public class ProcessResultState 
    {
        #region Fields
        public static ProcessResultState Success { get; } = new ProcessResultState(ProcessResultStateEnum.Success);
        public static ProcessResultState Fail { get; } = new ProcessResultState(ProcessResultStateEnum.Fail);
        public static ProcessResultState Running { get; } = new ProcessResultState(ProcessResultStateEnum.Running);
        public static ProcessResultState NeverRun { get; } = new ProcessResultState(ProcessResultStateEnum.NeverRun);
        private readonly ProcessResultStateEnum value;
        #endregion
        #region Constructors
        private ProcessResultState(ProcessResultStateEnum value)
        {
            this.value = value;
        }
        #endregion

        #region Methods
        public static bool operator ==(ProcessResultState prs1, ProcessResultState prs2)
        {
            return prs1?.value == prs2?.value;
        }

        public static bool operator !=(ProcessResultState prs1, ProcessResultState prs2)
        {
            return prs1?.value != prs2?.value;
        }

        public override bool Equals(object obj)
        {
            return this == (obj as ProcessResultState);
        }

        public override int GetHashCode()
        {
            return value.GetHashCode();
        }

        public static implicit operator ProcessResultState(bool success)
        {
            return success ? Success : Fail;
        }

        public static implicit operator string(ProcessResultState state)
        {
            return state.value.ToString();
        }
        #endregion

        private enum ProcessResultStateEnum
        {
            Success,
            Fail,
            Running,
            NeverRun
        }
    }
}