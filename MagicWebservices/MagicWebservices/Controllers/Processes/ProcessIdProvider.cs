﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MagicWebservices.Controllers.Processes
{
    public static class ProcessIdProvider
    {
        private static int lastProcessId = 0;

        public static int GenerateProcessId()
        {
            return lastProcessId++;
        }
    }
}