﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MagicWebservices.Controllers.Processes
{
    public interface IProcessResult
    {
        string Name { get; }
        ProcessResultState State { get; }
        Exception Error { get; }
        string ResultMessage();
    }
}
