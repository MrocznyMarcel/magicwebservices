﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

namespace MagicWebservices.Controllers.Processes
{
    public class ProcessGroupResult
    {
        #region Fields
        private readonly List<IProcessResult> processResults = new List<IProcessResult>();
        #endregion
        #region Properties
        public string ProcessGroupName { get; }

        #endregion
        #region Constructors
        public ProcessGroupResult(string groupName)
        {
            this.ProcessGroupName = groupName;
        }
        #endregion
        #region Methods
        public void AddResult(IProcessResult result)
        {
            this.processResults.Add(result);
        }

        public string GroupResultMessage()
        {
            var raportBuilder = new StringBuilder().AppendLine($"Results for group '{ProcessGroupName}'");
            foreach (var processResult in processResults)
                raportBuilder.AppendLine(processResult.ResultMessage());
            if (processResults.All(x => x.State == ProcessResultState.Success))
            {
                raportBuilder.AppendLine("All processes ended with sucess.");
                return raportBuilder.ToString();
            }
            if (processResults.Any(x => x.State == ProcessResultState.Running))
                raportBuilder.AppendLine("Some processes are still running.");
            if (processResults.Any(x => x.State == ProcessResultState.Fail))
                raportBuilder.AppendLine("Some processes failed.");
            return raportBuilder.ToString();
        }
        #endregion
    }
}