﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MagicWebservices.Controllers.Processes
{
    public class ProcessesGroup : ICollection<IProcess>
    {
        #region Fields
        private List<IProcess> processes = new List<IProcess>();
        private string groupName;
        #endregion
        #region Properties
        public string GroupName => groupName;
        public int Count => processes.Count;
        public bool IsReadOnly => false;
        #endregion

        #region Constructors
        public ProcessesGroup(string groupName)
        {
            this.groupName = groupName;
        }
        #endregion
        #region Methods
        public ProcessGroupResult ExecuteAll()
        {
            var results = new ProcessGroupResult(groupName);
            foreach (var process in processes)
                results.AddResult(process.Execute());
            return results;
        }

        public ProcessGroupResult GetCurrentResults()
        {
            var results = new ProcessGroupResult(groupName);
            foreach (var process in processes)
                results.AddResult(process.CurrentResult);
            return results;
        }

        public IEnumerator<IProcess> GetEnumerator()
        {
            return processes.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return processes.GetEnumerator();
        }

        public void Add(IProcess item)
        {
            processes.Add(item);
        }

        public void Clear()
        {
            processes.Clear();
        }

        public bool Contains(IProcess item)
        {
            return processes.Any(x => x.ProcessId == item.ProcessId);
        }

        public void CopyTo(IProcess[] array, int arrayIndex)
        {
            for (int i = arrayIndex, j = 0; i < array.Length; i++, j++)
                array[i] = processes[j];
        }

        public bool Remove(IProcess item)
        {
            return processes.Remove(item);
        }
        #endregion
    }
}