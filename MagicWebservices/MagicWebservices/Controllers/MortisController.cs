﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http.Cors;
using System.Web.Mvc;
using MagicWebservices.Controllers.Mortis;
using MagicWebservices.Controllers.Mortis.Database;
using MagicWebservices.Controllers.Mortis.NewpointsShopManager;
using MagicWebservices.Controllers.Processes;

namespace MagicWebservices.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MortisController : Controller
    {
        #region Fields
        private static ProcessesGroup fiveMinutesProcesses;
        private static ProcessesGroup twentyFourHoursProcesses;
        #endregion
        #region Methods

        [System.Web.Http.HttpGet]
        public JsonResult GetAllUsers()
        {
            List<UserNameId> userNameIds = new List<UserNameId>();
            using (var database = new MortisModelContainer())
            {
                userNameIds.AddRange(database.mybb_users.Select(u => new UserNameId
                {
                    UserId = u.uid,
                    Username = u.username
                }).OrderBy(u => u.Username));
            }
            Response.AppendHeader("Access-Control-Allow-Origin", "*");
            return new JsonResult
            {
                ContentEncoding = Encoding.UTF8,
                Data = userNameIds,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };
        }

        private class UserNameId
        {
            public string Username { get; set; }
            public long UserId { get; set; }
        }

        [System.Web.Http.HttpGet]
        public ActionResult RebuildSpells()
        {
            return View("~/Views/Mortis/ShowReport.cshtml", MortisTasks.OnDemand.RebuildSpells());
        }

        [System.Web.Http.HttpGet]
        public ActionResult ChangePostAuthor(long postId, int newUserId)
        {
            return View("~/Views/Mortis/ShowReport.cshtml", MortisTasks.OnDemand.ChangePostAuthor(postId, newUserId));
        }

        private void InitializeFiveMinutesProcessesGroup()
        {
            fiveMinutesProcesses = new ProcessesGroup("FiveMinutesProcesses")
            {
                new SingleProcess(MortisTasks.FiveMinutes.UpdateProfileShopItems),
                new SingleProcess(MortisTasks.FiveMinutes.UpdateDepositLinks),
                new SingleProcess(MortisTasks.FiveMinutes.UpdateOwlMailLinks),
                new SingleProcess(MortisTasks.FiveMinutes.UpdateDiaryLinks)
            };
        }

        private void InitializeTwentyFourHoursProcessesGroup()
        {
            twentyFourHoursProcesses = new ProcessesGroup("TwentyFourHoursProcesses")
            {
                new SingleProcess(MortisTasks.TwentyFourHours.DeletePostsOlderThanTwoWeeksInThread)
            };
        }


        [System.Web.Http.HttpGet]
        public ActionResult ExecuteFiveMinutesProcesses()
        {
            if (fiveMinutesProcesses == null)
                InitializeFiveMinutesProcessesGroup();
            fiveMinutesProcesses.ExecuteAll();
            return RedirectToAction("FiveMinutesProcessesReport");
        }

        [System.Web.Http.HttpGet]
        public ActionResult ExecuteFiveMinutesProcessesApi()
        {
            if (fiveMinutesProcesses == null)
                InitializeFiveMinutesProcessesGroup();
            return View("~/Views/Mortis/ProcessesReport.cshtml", fiveMinutesProcesses.ExecuteAll());
        }

        [System.Web.Http.HttpGet]
        public ActionResult FiveMinutesProcessesReport()
        {
            if (fiveMinutesProcesses == null)
                InitializeFiveMinutesProcessesGroup();
            return View("~/Views/Mortis/ProcessesReport.cshtml", fiveMinutesProcesses.GetCurrentResults());
        }

        [System.Web.Http.HttpGet]
        public ActionResult ExecuteTwentyFourHoursProcesses()
        {
            if (twentyFourHoursProcesses == null)
                InitializeTwentyFourHoursProcessesGroup();
            twentyFourHoursProcesses.ExecuteAll();
            return RedirectToAction("TwentyFourHoursProcessesReport");
        }

        [System.Web.Http.HttpGet]
        public ActionResult ExecuteTwentyFourHoursProcessesApi()
        {
            if (twentyFourHoursProcesses == null)
                InitializeTwentyFourHoursProcessesGroup();
            return View("~/Views/Mortis/ProcessesReport.cshtml", twentyFourHoursProcesses.ExecuteAll());
        }

        [System.Web.Http.HttpGet]
        public ActionResult TwentyFourHoursProcessesReport()
        {
            if (twentyFourHoursProcesses == null)
                InitializeTwentyFourHoursProcessesGroup();
            return View("~/Views/Mortis/ProcessesReport.cshtml", twentyFourHoursProcesses.GetCurrentResults());
        }

        [System.Web.Http.HttpGet]
        public ActionResult NewpointsShopManager(string loginKey)
        {
            NewpointsShopManagerData data = NewpointsShopManagerData.GetNewpointsShopManagerDataForToken(loginKey);
            return data.CurrentUserPermissions == NewpointsManagerPermissions.None 
                ? View("~/Views/Mortis/NewpointsShopManagerNoPermissions.cshtml", data) 
                : View("~/Views/Mortis/NewpointsShopManager.cshtml", data);
        }

        [System.Web.Http.HttpPost]
        public ActionResult ChangePointsValue(NewpointsShopManagerData data)
        {
            data.PointsChange.ApplyPointsChange();
            return RedirectToAction("NewpointsShopManager", new { loginKey = data.CurrentUserUidAndLoginKey });
        }

        [System.Web.Http.HttpPost]
        public ActionResult ChangeSpells(NewpointsShopManagerData data)
        {
            data.SpellsChange.ApplySpellsChange();
            return RedirectToAction("NewpointsShopManager", new { loginKey = data.CurrentUserUidAndLoginKey });
        }
        #endregion
    }
}