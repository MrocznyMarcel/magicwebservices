﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using CG.Web.MegaApiClient;

namespace MagicWebservices.Controllers.Utilities
{
    public class MegaDriveConnector : IDisposable
    {
        private MegaApiClient client;
        public MegaDriveConnector(string email, string password)
        {
            client = new MegaApiClient();
            client.Login(email, password);
        }

        public void Upload(Stream stream, string fileName, string megaFolderId)
        {
            INode folderNode = client.GetNodes().First(x => x.Id == megaFolderId);
            client.Upload(stream, fileName, folderNode);
        }

        public void Dispose()
        {
            if (client.IsLoggedIn)
                client.Logout();
        }
    }
}