﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MagicWebservices.Models
{
    public class Report
    {
        public string Name { get; set; }
        public string Data { get; set; }
    }
}