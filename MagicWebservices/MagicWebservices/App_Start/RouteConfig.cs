﻿using System.Web.Mvc;
using System.Web.Routing;

namespace MagicWebservices
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{loginKey}",
                defaults: new { controller = "Home", action = "Index", loginKey = UrlParameter.Optional }
            );
        }
    }
}
